#!/bin/bash

set -e
set -u

### IMPORTANT ###
###  VERSION  ###
my_name=walnut
my_app_pkg_name=com.daplie.walnut.web
my_app_ver="v1.2"
my_azp_oauth3_ver="v1.2"
export NODE_VERSION="v8.9.0"

if [ -z "${my_tmp-}" ]; then
  my_tmp="$(mktemp -d)"
  mkdir -p $my_tmp/opt/$my_name/core
  echo "Installing to $my_tmp (will be moved after install)"
  git clone ./ $my_tmp/opt/$my_name/core
  pushd $my_tmp/opt/$my_name/core
fi

#################

### IMPORTANT ###
###  VERSION  ###
#my_app_ver="v1.1"
my_app_ver="installer-v2"
my_launchpad_ver="v1.2"
my_azp_oauth3_ver="v1.1.3"
my_iss_oauth3_rest_ver="v1.2.0"
my_iss_oauth3_pages_ver="v1.2.1"
my_www_daplie_ver=v1.0.15
export NODE_VERSION="v8.9.0"
#################
export NODE_PATH=$my_tmp/opt/$my_name/lib/node_modules
export PATH=$my_tmp/opt/$my_name/bin/:$PATH
export NPM_CONFIG_PREFIX=$my_tmp/opt/$my_name
my_npm="$NPM_CONFIG_PREFIX/bin/npm"
#################



# TODO un-hardcode core at al
#my_app_dist=$my_tmp/opt/$my_name/lib/node_modules/$my_name/dist
my_app_dist=$my_tmp/opt/$my_name/core/dist
installer_base="https://git.daplie.com/Daplie/goldilocks.js/raw/$my_app_ver"

# Backwards compat
# some scripts still use the old names
my_app_dir=$my_tmp
my_app_name=$my_name



git checkout $my_app_ver

mkdir -p $my_tmp/{etc,opt,srv,var}/$my_name
mkdir -p "$my_tmp/var/log/$my_name"
mkdir -p "$my_tmp/opt/$my_name"/{bin,config,core,etc,lib,node_modules,var}
ln -s ../core/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name
ln -s ../core/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name.js
#ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name
#ln -s ../lib/node_modules/$my_name/bin/$my_name.js $my_tmp/opt/$my_name/bin/$my_name.js
mkdir -p "$my_tmp/opt/$my_name"/packages/{api,pages,rest,services}
mkdir -p "$my_tmp/opt/$my_name"/etc/client-api-grants
# TODO move packages and sites to /srv, grants to /etc
ln -s ../etc/client-api-grants "$my_tmp/opt/$my_name"/packages/client-api-grants
mkdir -p "$my_tmp/opt/$my_name"/var/sites
ln -s ../var/sites "$my_tmp/opt/$my_name"/packages/sites
mkdir -p "$my_tmp/etc/$my_name"
chmod 775 "$my_tmp/etc/$my_name"
cat "$my_app_dist/etc/$my_name/$my_name.example.yml" > "$my_tmp/etc/$my_name/$my_name.example.yml"
chmod 664 "$my_tmp/etc/$my_name/$my_name.example.yml"
mkdir -p $my_tmp/var/log/$my_name



#
# Helpers
#
source ./installer/sudo-cmd.sh
source ./installer/http-get.sh



#
# Dependencies
#
echo $NODE_VERSION > /tmp/NODEJS_VER
# This will read the NODE_* and PATH variables set previously, as well as /tmp/NODEJS_VER
http_bash "https://git.coolaj86.com/coolaj86/node-installer.sh/raw/v1.1/install.sh"
$my_npm install -g npm@4
$my_npm install -g bower
touch $my_tmp/opt/$my_name/.bowerrc
echo '{ "allow_root": true }' > $my_tmp/opt/$my_name/.bowerrc

#pushd $my_tmp/opt/$my_name/lib/node_modules/$my_name
pushd $my_tmp/opt/$my_name/core
  mkdir -p ../node_modules
  ln -s ../node_modules node_modules
  $my_npm install
popd

git clone https://git.daplie.com/Daplie/walnut_launchpad.git $my_tmp/opt/$my_name/core/lib/walnut@daplie.com/setup
pushd $my_tmp/opt/$my_name/core/lib/walnut@daplie.com/setup
  git pull
  git checkout $my_launchpad_ver

  git clone https://git.daplie.com/OAuth3/oauth3.js.git ./assets/oauth3.org
  pushd assets/oauth3.org
    git checkout $my_azp_oauth3_ver
  popd
popd

pushd $my_tmp/opt/$my_name/packages
  git clone https://git.daplie.com/OAuth3/issuer_oauth3.org.git rest/issuer@oauth3.org
  pushd rest/issuer@oauth3.org/
      git checkout $my_iss_oauth3_rest_ver
      $my_npm install
  popd

  git clone https://git.daplie.com/OAuth3/org.oauth3.git pages/issuer@oauth3.org
  pushd pages/issuer@oauth3.org
    git checkout $my_iss_oauth3_pages_ver
    bash ./install.sh

    pushd ./assets/oauth3.org
      git checkout $my_azp_oauth3_ver
    popd
  popd

  git clone https://git.daplie.com/Daplie/walnut_rest_www_daplie.com.git rest/www@daplie.com
  pushd rest/www@daplie.com
    git checkout $my_www_daplie_ver
    $my_npm install
  popd
popd



#
# System Service
#
source ./installer/my-root.sh
echo "Pre-installation to $my_tmp complete, now installing to $my_root/ ..."
set +e
if type -p tree >/dev/null 2>/dev/null; then
  #tree -I "node_modules|include|share" $my_tmp
  tree -L 6 -I "include|share|npm" $my_tmp
else
  ls $my_tmp
fi
set -e

source ./installer/my-user-my-group.sh
echo "User $my_user Group $my_group"

$sudo_cmd chown -R $my_user:$my_group $my_tmp
$sudo_cmd chown root:root $my_tmp/*
$sudo_cmd chown root:root $my_tmp
$sudo_cmd chmod 0755 $my_tmp
$sudo_cmd rsync -a --ignore-existing $my_tmp/ $my_root/
$sudo_cmd rsync -a --ignore-existing $my_app_dist/etc/$my_name/$my_name.yml $my_root/etc/$my_name/$my_name.yml
source ./installer/install-system-service.sh

# Change to admin perms
$sudo_cmd chown -R $my_user:$my_group $my_root/opt/$my_name
$sudo_cmd chown -R $my_user:$my_group $my_root/var/www $my_root/srv/www

# make sure the files are all read/write for the owner and group, and then set
# the setuid and setgid bits so that any files/directories created inside these
# directories have the same owner and group.
$sudo_cmd chmod -R ug+rwX $my_root/opt/$my_name
find $my_root/opt/$my_name -type d -exec $sudo_cmd chmod ug+s {} \;



echo ""
echo "You must have some set of domain set up to properly use goldilocks+walnut:"
echo ""
echo "  example.com"
echo "  www.example.com"
echo "  api.example.com"
echo "  assets.example.com"
echo "  cloud.example.com"
echo "  api.cloud.example.com"
echo ""
echo "Check the WALNUT README.md for more info and how to set up /etc/goldilocks/goldilocks.yml"
echo ""
echo "Unistall: rm -rf /srv/walnut/ /var/walnut/ /etc/walnut/ /opt/walnut/ /var/log/walnut/ /etc/systemd/system/walnut.service /etc/tmpfiles.d/walnut.conf"



rm -rf $my_tmp
