'use strict';

/**
 * @ngdoc service
 * @name steve.StApi
 * @description
 * # StApi
 * Service in the steve.
 */
(function () {

  var x = window.StClientConfig
    ;

  x.googleAnalyticsToken = window.googleAnalyticsToken || 'UA-XXXXXXXX-1';
  // window.StApi

  x.loginConfig = x.loginConfig || { requireLocalLogin: true, minLogins: 1  };
  x.accountConfig = x.accountConfig || {};

  angular.module('steve', [])
    .constant('stConfig', x)
    .service('StApi', function StApi() {
      // AngularJS will instantiate a singleton by calling "new" on this function
      var me = this
        ;

      Object.keys(x).forEach(function (k) {
        me[k] = x[k];
      });
    });
}());
