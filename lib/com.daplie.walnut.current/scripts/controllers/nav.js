'use strict';

angular.module('yololiumApp')
  .controller('NavController', [
    '$scope'
  , '$http'
  , 'DaplieApiConfig'
  , 'DaplieApiSession'
  , function ($scope, $http, DaplieApiConfig, DaplieApiSession) {
    var scope = this;

    function init(session) {
      if (!session || session.message) {
        scope.session = null;
        scope.account = null;
        scope.accounts = null;
        return;
      }

      scope.session = session;
      scope.accounts = session.accounts;
      scope.account = DaplieApiSession.account(session);
    }

    scope.showLoginModal = function () {
      // TODO profile manager
      return DaplieApiSession.openAuthorizationDialog();
    };

    scope.logout = function () {
      // TODO which token(s) to destroy?
      return DaplieApiSession.logout();
    };

    DaplieApiSession.checkSession().then(init, init).catch(init);
    DaplieApiSession.onLogin($scope, init);
    DaplieApiSession.onLogout($scope, init);
  }]);
