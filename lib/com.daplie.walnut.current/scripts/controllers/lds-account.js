'use strict';

angular.module('yololiumApp')
  .controller('LdsAccountController', [
    '$scope'
  , '$q'
  , '$timeout'
  , '$http'
  , '$modalInstance'
  , 'realLdsAccount'
  , 'DaplieApiConfig'
  , 'DaplieApiSession'
  , 'mySession'
  , 'myProfile'
  , 'myOptions'
  , function (
      $scope
    , $q
    , $timeout
    , $http
    , $modalInstance
    , LdsAccount // prevent circular reference
    , DaplieApiConfig
    , DaplieApiSession
    , account // session doubles as account
    , profile
    //, opts
    ) {
    var scope = this;

    scope.me = profile.me;

    console.log("DEBUG xyz-account profile", profile);

    scope.markAsChecked = function () {
      console.log('DEBUG mark as checked account');
      console.log(account);
      return $http.post(
        DaplieApiConfig.providerUri + '/api/io.lds/accounts/' + account.id + '/mark-as-checked'
      , null
      , { headers: { 'Authorization': 'Bearer ' + account.token } }
      ).then(function (resp) {
        if (!resp.data || resp.data.error || !resp.data.success) {
          scope.flashMessage = (resp.data && resp.data.error) || "Failed to mark account as checked.";
          scope.flashMessageClass = 'alert-danger';
          return;
        }

        account.userVerifiedAt = new Date().toISOString();

        // pass back anything?
        return $modalInstance.close();
      });
    };
  }]);
