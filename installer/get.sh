set -e
set -u

my_name=walnut
# TODO provide an option to supply my_ver and my_tmp
my_ver=master
my_tmp=$(mktemp -d)

mkdir -p $my_tmp/opt/$my_name/lib/node_modules/$my_name
git clone https://git.daplie.com/Daplie/walnut.js.git $my_tmp/opt/$my_name/core

echo "Installing to $my_tmp (will be moved after install)"
pushd $my_tmp/opt/$my_name/core
  git checkout $my_ver
  source ./installer/install.sh
popd

echo "Installation successful, now cleaning up $my_tmp ..."
rm -rf $my_tmp
echo "Done"
