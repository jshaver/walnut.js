(function () {
  'use strict';

  // getProofOfSecret(salt, secret, iter)
  function getProofOfSecret(nodeObj) {
    // TODO test correctness
    console.info('TODO test correctness of getProofOfSecret');
    var d = $q.defer();
    var kdf = {
      node: nodeObj.node
    , type: nodeObj.type
    , kdf: 'PBKDF2'
    , algo: 'SHA-256'
    };

    // generate a password-based 16-byte key
    // note an optional message digest can be passed as the final parameter
    if (nodeObj.salt) {
      kdf.salt = Unibabel.bufferToBinaryString(Unibabel.hexToBuffer(nodeObj.salt));
    } else {
      // uses binary string
      kdf.salt = forge.random.getBytesSync(32);
    }
    kdf.iter = nodeObj.iter || Math.floor(Math.random() * 1000) + 1000;
    kdf.byteLen = nodeObj.byteLen || 16;

    console.log('kdf.salt', kdf.salt);

    // kdf.proof = forge.pkcs5.pbkdf2(nodeObj.secret, kdf.salt, kdf.iter, kdf.byteLen);

    // generate key asynchronously
    // note an optional message digest can be passed before the callback
    forge.pkcs5.pbkdf2(nodeObj.secret, kdf.salt, kdf.iter, kdf.byteLen, 'sha256', function(err, derivedKey) {
      // do something w/derivedKey
      if (err) {
        d.reject(err);
        return;
      }

      kdf.salt = Unibabel.bufferToHex(Unibabel.binaryStringToBuffer(kdf.salt));
      kdf.proof = Unibabel.bufferToHex(Unibabel.binaryStringToBuffer(derivedKey));
      console.log('kdf', kdf);
      d.resolve(kdf);
    });

    return d.promise;
  }

}());
